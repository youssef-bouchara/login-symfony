<?php

namespace AppBundle\Controller;

use FOS\UserBundle\FOSUserBundle;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        /*
        $em=$this->getDoctrine()->getManager();
        $user=$this->getDoctrine()->getRepository("AppBundle:User")->findOneBy(array('username' => 'othmane'));
        $user->addRole("ROLE_ADMIN");
        $em->persist($user);
        $em->flush();*/

        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
        ]);
    }
}
